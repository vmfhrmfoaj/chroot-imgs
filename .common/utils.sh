#!/bin/bash

function schroot_template {
  name=${1}
  dir=${2}
  user=${3}
  echo "[${name}]
description=Development envrironment for ${name}
type=directory
directory=${dir}
profile=default
groups=${user}
root-users=${user}"
}
