#!/bin/bash -x

# for ZSH
touch /etc/profile.d/dummy.sh
chmod -R 755 /usr/local/share/zsh
chown -R root:root /usr/local/share/zsh

# for apt-get
rm -rf /var/lib/dpkg/statoverride
touch /var/lib/dpkg/statoverride
